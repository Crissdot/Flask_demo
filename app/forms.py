from wtforms import Form, HiddenField
from wtforms import validators
from wtforms import StringField, PasswordField, BooleanField, TextAreaField
from wtforms.fields.html5 import EmailField

from .models import User

def codi_validator(form, field):
    if field.data == 'codi' or field.data == 'Codi':
        raise validators.ValidationError('El username Codi no es permitido')

def length_honeypot(form, field):
    if len(field.data) > 0:
        raise validators.ValidationError('Solo los humanos puede completar el registro!')

class LoginForm(Form):
    username = StringField('Username', [
        validators.length(min=4, max=50, message='El username debe contener 4 o más caracteres')
    ])
    password = PasswordField('Password', [
        validators.Required(message='La contraseña es requerida')
    ])

class RegisterForm(Form):
    username = StringField('Username', [
        validators.length(min=4, max=50),
        codi_validator
    ])
    email = EmailField('Email', [
        validators.length(min=6, max=100),
        validators.Required(message='El email es requerido'),
        validators.Email(message='Ingrese un email válido')
    ])
    password = PasswordField('Password', [
        validators.Required('La contraseña es requerida'),
        validators.EqualTo('confirm_password', message='Las contraseñas no coinciden')
    ])
    confirm_password = PasswordField('Confirme la contraseña')
    accept = BooleanField('Acepto tétminos y condiciones', [
        validators.DataRequired()
    ])
    honeypot = HiddenField("", [ length_honeypot ])

    def validate_username(self, username):
        if User.get_by_username(username.data):
            raise validators.ValidationError('El username ya se encuentra en uso')

    def validate_email(self, email):
        if User.get_by_email(email.data):
            raise validators.ValidationError('El email ya se encuentra en uso')

    def validate(self):
        if not Form.validate(self):
            return False
        
        if len(self.password.data) < 3:
            self.password.errors.append('El password es demasiado corto')
            return False

        return True

class TaskForm(Form):
    title = StringField('Título', [
        validators.length(min=4, max=50, message='Título fuera de rango'),
        validators.DataRequired(message='El título es requerido')
    ])
    description = TextAreaField('Descripción', [
        validators.DataRequired(message='La descripción es requerida')
    ], render_kw={'rows': 5})
